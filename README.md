### Corpus Publication Classifier

Perl Script to Classify Publication Topics into Story/Other/Scores.

### Description: 
	The program basically takes data that is in near GW format and processes
	it to find the story type "that is to classify it" and the subsequently
	process it further to remove any empty <TEXT> tags amd also P tags 
	containing data that is corrupt.
	
### Concept: 
   Numerical Pattern evaluation based on spaces,words and number of paragraphs

### Procedure:
   Read whole file document by document  	
   Process each doc seperately
   Assign the type of the story 		
   Clean up the document for any unnecessary paragraphs
   Finally output the header, the dateline and the text 		   	
	followed with the closing tags.

The Statistical Algorithm used certain text information to discriminatively determine the type. An example below shows a document that is not a news story but a other - score type.

```
<DOC  id = "APW_FRE_19940512.0002"  kind = "other">
<HEADLINE>
ATP - Coral Springs - 4th day: the results
</HEADLINE>
<TEXT>

   CORAL SPRINGS (USA), May 12 (APW) - Results Thursday recorded 
at the 4th day of Coral Springs tennis tournament (Florida) test 
ATP Tour with $ 240,000. 
   
   Men's Singles (2nd round):
   Morgan Jamie (Aus / N7) beat Juan Garat (Arg) 6-1, 6-2
   Mark Woodforde (Aus) beat Jimmy Arias (USA) 6-3, 6-4
   Jared Palmer (USA) beat Marcelo Ingaramo (Arg) 6-1, 6-2
   
</TEXT>
</DOC>
```
