#!/usr/bin/perl

use strict;
use warnings;
use IPC::Open2;
sub paragraph
{

    my ($text,$spaces,$length,$numberOfWords)=@_;
    my %paragraph=(
        "textOfPara" => $text,
        "spaceCount" => $spaces,
        "lengthOfPara"=> $length,
        "numberOfWords"=>$numberOfWords
        );
    return \%paragraph;
}
open(FILE,"<",$ARGV[0]);
my $doc="";
$/="</DOC>\n";

while(<FILE>)
{

    my @spaceForSelect;
    my $averageSpace=0;
    my @spaceArray;
    my @divs;
    my @allParas;
    my @allParasOfStory;
    my $headline="";
    my $numberOfParas=0;
    my $docId="";
    my @lengthArray;
    my $paraCounter=0;
    my %paraLength;
    my %spaceNumber;
    my %wordsNumber;
    my $numberOfWords=0;
    my $docForLI;
    my $languageScore;
    my @paraSizeArray;
    my $type="";
    my $documentParas="";
    my $finalDoc="";
    my $formtheHeadLine="";
    my $StoryTag;
    my $checkPostDL="";

    #AFP SPECIFIC PARAMETER
    #keep a count if the document is a multi type
    my $multiCounter=0;
    #keep a count if the document is a other  type
    my $otherCounter=0;
    $doc=$_;
    #print $doc;
    /((\w){3}_(\w){3}_(\d){8}\.(\d){4}\"\s+)/;
    $docId=$1;

    #THIS HANDLES PROCESSING FOR FILES FROM 01-06 AS DAVE HAS ALREADY DONE THAT PART
    my $typeAlreadyAssigned="";

    if($docId=~m/20070[1-6]/)
    {
    ($typeAlreadyAssigned)=$doc=~/(type=\"(other|story|advis|multi)\"\s*>\n*)/;
    #$typeAlreadyAssigned=$1;
    #print $typeAlreadyAssigned;
    }


    my $documentHeader="<DOC id=\"$docId";

    my ($documentHeadline)=/<HEADLINE>\n(.*)\n<\/HEADLINE>/s;
    my $tempText="";

    my ($documentDateline)=/<DATELINE>\n*(.*)\n*<\/DATELINE>/s;

    my ($allText)=/<TEXT>((.|\n)*)<\/TEXT>/;

    if(!defined $documentDateline)
    {
	#$tempText=/<TEXT>((.|\n)*)<\/TEXT>/;
	($documentDateline)=/((.|\n)*\(AFP\))\s+(-){1,}/;
	if(defined $documentDateline)
        {
	    my @dateline=split("<TEXT>",$documentDateline);
	    if(@dateline)
	    {
		$dateline[$#dateline]=~s/\n//g;
		#print "$headline[$#headline]";
		$documentDateline=$dateline[$#dateline];
		$checkPostDL="true";
	    }
	} 
    }
    else
    {	
	$checkPostDL="false";
	$documentDateline=~s/\n//g;
    }

    my $documentEndTags="</TEXT>\n</DOC>\n";
    my $entireText;

    my $pipeID = open2(\*POUT, \*PIN, '/home/mendonca/LanguageModel/Language_Id/src/li','/home/mendonca/LanguageModel/SpEnBeFrDe.Model');
    print PIN;
    close PIN;
    {
        local $/;
        $languageScore=<POUT>;
    }
    close POUT;
    # print $languageScore;
    my($language,$score)=split('\s',$languageScore);
    #print "$score\n";

    if(/(<P>(.|\n)*<\/P>)/)
    {
	@allParasOfStory = split(/<\/P>/, $1);
	$numberOfParas= @allParasOfStory;
	#print "\t\t\t\t\t\tNUMBER OF PARAS $numberOfParas for $docId";
    }
    if($numberOfParas!=0)
    {

	foreach my $singlePara (@allParasOfStory) 
	{

	    #SPACES IN THE STRING
	    my $spaceCount = ($singlePara =~ tr/ //);

	    #LENGTH OF THE STRING

	    $singlePara=~s/<\/P>|<P>//g;
	    #print $singlePara;
	    chomp($singlePara);

	    my @words=split(/\s+/, $singlePara);
	    $numberOfWords=@words;
	    $paraSizeArray[$paraCounter]=$numberOfWords;
	    chomp($singlePara);

	    # $lengthArray[$singlePara]=length($singlePara);

	    $allParas[$paraCounter]=paragraph($singlePara,$spaceCount,$lengthArray[$paraCounter],$numberOfWords);
	    #$paraLength{$lengthArray[$paraCounter]}++;
	    #$spaceNumber{$i}=$spaceCount;
	    $wordsNumber{$numberOfWords}++;
	    $paraCounter++;
	}

	while ( my ($k,$v) = each %wordsNumber ) {
	    #print "$k => $v\n";
	}
	my @sorted= sort{$wordsNumber{$b} <=> $wordsNumber{$a}} keys %wordsNumber;
	#print "$wordsNumber{$sorted[0]} paras have equal number of words from $paraCounter paragraphs\n";
        # PRINT THE HASH OF WORD COUNTS

	#NUMBER OF DISTINCT PARAGRAPH LENGTHS
	my $distinctWordCount=scalar keys %wordsNumber;
	#print "\n\t\t\t\t\t\t###########DIAGNOSTICS FOR STORY TAGGING###########\n";
	#print "\t\t\t\t\t1.THERE ARE  $distinctWordCount DIFFERENT PARA LENGTHS FOR $paraCounter PARAGRAPHS\n";

	# FIND THE AVERAGE WORD COUNT FOR THE PARAGRAPHS
	my $totalNumberOfWords=0;
	my $averageWordLength=0;

	while( my ($k, $v) = each %wordsNumber ) {
	    $totalNumberOfWords=$totalNumberOfWords+$k;
	}	
	$averageWordLength=$totalNumberOfWords/$paraCounter;
	my $paraToWordRatio=$distinctWordCount/$paraCounter;
	#print "\t\t\t\t\t2.THE AVERAGE WORD LENGTH IS ".$averageWordLength."\n";


	foreach my $singlePara (@allParas)
        {

	    if($singlePara->{"textOfPara"}=~m/=={2,}$/)
	    {
		$multiCounter++;
	    }
	    elsif($singlePara->{"textOfPara"}=~m/^--/m)
	    {
		$multiCounter++;
	    }
	    elsif($singlePara->{"textOfPara"}=~m/^-\s+\d*/m)
	    {
		#print $singlePara->{"textOfPara"};
		$multiCounter++;
	    }	
	    elsif($singlePara->{"textOfPara"}!~m/\.$/)
	    {
		$otherCounter++;
	    }	

        }
	
        if($docId!~m/20070[1-6]/)
        {
	    if(($paraToWordRatio>=0.4)&&($averageWordLength>=10)&&($score>=25))
	    {
		#print "\t\t\t\t\t\tResult. Type = story for DOCID:".$docId;
		$type="story";
	    }
	    elsif(($paraToWordRatio>=0.4)&&($averageWordLength>=10))
	    {
		#print "\t\t\t\t\t\tResult. Type = story for DOCID:".$docId;
		$type="story";
	    }
	    elsif(($paraToWordRatio>=0.4)&&($averageWordLength<=10)&&($score<=25))
	    {
		#print "\t\t\t\t\t\tResult. Type = story for DOCID:".$docId;
		$type="other";
	    }

	    elsif(($paraCounter==1)&&($paraSizeArray[0]>=10)&&($score>=25))
	    {
		#print "\t\t\t\t\t\tResult. Type = other for DOCID:".$docId;
		$type="other";
	    }
	    elsif($averageWordLength<=5&&($score<25))
	    {
		#print "\t\t\t\t\t\tResult. Type = other for DOCID:".$docId;
		$type="other";
	    }
	    elsif($multiCounter>=1)
	    {
		#print "\t\t\t\t\t\tResult. Type = multi for DOCID:".$docId;
		$type="multi";
	    }
	    elsif(($wordsNumber{$sorted[0]}/$paraCounter>=0.5))
	    {
		#print "\t\t\t\t\t\tResult. Type = other for DOCID:".$docId;
		$type="other";
	    }
	    elsif(($otherCounter>=2)&&($score<=25)&&($averageWordLength>=5)&&($averageWordLength<=10))
	    {
		$type="other";
	    }
	    elsif(($otherCounter>=2)&&($score<=25)&&($averageWordLength>=10))
	    {
		$type="other";
	    }
	    else
	    {
		#print "I am in the end of else ";
		my @pars = ( $doc =~ m{<P>\n(.*?)</P>}gs );
		s/\n//g for ( @pars );
		my $ntabbed = grep { /^\d/ or /\t/ or / {3,}/ or /:$/ or /\d[\).,]?$/} @pars;
		$type = ( $ntabbed > @pars / 2 ) ?  "other" : "story";
	    }   
	}#MAKING STORIES TAG FOR ELEMENTS OUT OF THE 200701-200706 RANGE
        else
        {
            ($type)=$doc=~/(type=\"(other|story|advis|multi)\"\s*>\n*)/;

        }

	$StoryTag="type=\"$type\">\n";
	if(defined $documentHeadline)
	{

	    $finalDoc=$documentHeader.$StoryTag."<HEADLINE>\n".$documentHeadline."\n</HEADLINE>\n";

	}
        else
        {
            $finalDoc=$documentHeader.$StoryTag;
        }

	if(defined $documentDateline)
	{
	    $finalDoc.="<DATELINE>\n".$documentDateline."\n</DATELINE>\n<TEXT>\n";
	    $documentDateline=quotemeta $documentDateline;
	    #$entireText=~s/$documentDateline//g;
	    #$finalDoc.=$entireText;
	}
	else
	{
	    $finalDoc.="<TEXT>\n";
	}
	my $paraArray=0;

	foreach my $singlePara (@allParas)
	{
	    my $paraLines=$singlePara->{"textOfPara"};

	   # if($paraArray==$#allParas)
	   # {
		my @div=split(" ",$paraLines);
		if(@div < 4 and $paraLines=~ m![/:()-]! and not $div[$#div-1] =~ /[.?!]$/)
		{
		    next;
		}

	   # }

	    $singlePara->{"textOfPara"}=~s/\n//g;;
	    $documentParas.="<P>\n".$singlePara->{"textOfPara"}."\n</P>\n";

	    $paraArray++;
	}
	# THIS SHOWS THAT THE PARAS HAD SOME PROBLEMS
	if($documentParas eq "")
	{
	    next;
	}
	$finalDoc.=$documentParas;
	$finalDoc.=$documentEndTags;


    }#END OF IF WHERE PARAS ARE EXISTING

  ##########ZERO PARAGRAPHS PROCESSING###########
    elsif($numberOfParas==0)
    {
	($entireText)=/<TEXT>((.|\n)*)<\/TEXT>/;
	# number of matching newlines
	my $newLineCount=()=$entireText=~m/\n/g;

	if($docId!~m/20070[1-6]/)
	{    
	    ($StoryTag)="type=\"ND\">\n";
	    if ( /^ \s* a \s+ l.attention \s+ de | \b \d+ \s+ mots \b /ix ) 
	    {
		$StoryTag ="type=\"advis\" >\n";
	    }
	    elsif($score<25)
	    {
		print $score;
		$StoryTag ="type=\"other\" >\n";
	    }
	    elsif ( $documentHeadline =~ /le monde en bref/i ) {
		$StoryTag ="type=\"multi\" >\n";
	    }
	    elsif(($score>=25)&&(!defined $StoryTag))
	    {
		$StoryTag ="type=\"story\" >\n";
	    }
	    
	}#MAKING STORIES TAG FOR ELEMENTS OUT OF THE 200701-200706 RANGE
	else
	{
	    ($StoryTag)=$doc=~/(type=\"(other|story|advis|multi)\"\s*>\n*)/;

	}
	$finalDoc.=$documentHeader.$StoryTag;
	
	
	if(defined $documentHeadline)
        {
            $finalDoc=$documentHeader.$StoryTag."<HEADLINE>\n".$documentHeadline."\n</HEADLINE>\n";
        }
	else
        {
	    $finalDoc=$documentHeader.$StoryTag;
        }
	if($checkPostDL eq "true")
	{
	    if(defined $documentDateline)
	    {
		$finalDoc.="<DATELINE>\n".$documentDateline."\n</DATELINE>\n<TEXT>\n";
		$documentDateline=quotemeta $documentDateline;
		#$entireText=~s/$documentDateline//g;
		$finalDoc.=$entireText;
	    }
	}
	elsif(($checkPostDL eq "false"))#if checkpostdl is false that means the dateline existed
	{
	    $finalDoc.="\n<DATELINE>\n".$documentDateline."\n</DATELINE>\n<TEXT>";
	    $finalDoc.=$entireText;
	}
       	
	$finalDoc.="\n</TEXT>\n</DOC>\n";
        #print $entireText;

    }


    #FORM  THE OUTPUT PARAS 

    if ( $type eq "multi" )
    {
	$finalDoc =~ s{(?:</?P>\s+)+}{\n}g;
    }
    elsif ( $type eq "other" )
    {
        $finalDoc =~ s{</?P>\s+}{}g;
    }
    else
    {
        $finalDoc =~ s{</P>\n{2,}}{</P>\n}g;
    }

   print $finalDoc;

}#END OF WHILE
close FILE;
